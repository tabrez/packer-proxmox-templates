packer {
  required_plugins {
    proxmox = {
      version = " >= 1.0.1"
      source  = "github.com/hashicorp/proxmox"
    }
  }
}

source "proxmox-iso" "ubuntu-server-2204" {
  proxmox_url = "https://${var.proxmox_hostname}:8006/api2/json"
  username         = "${var.proxmox_user}"
  password         = "${var.proxmox_password}"
  # token            = "${var.proxmox_token}"
  node             = "${var.proxmox_node}"

  template_name        = "${var.template_name}"
  template_description = "${var.template_description}"
  iso_urls = "${var.iso_urls}"
  iso_checksum = "${var.iso_checksum}"
  unmount_iso          = true
  iso_storage_pool = "local"

  ssh_username           = "ubuntu"
  ssh_password           = "${var.ssh_password}"
  ssh_private_key_file   = pathexpand("${var.ssh_private_key_path}")
  ssh_timeout            = "30m"
  ssh_pty                = true

  insecure_skip_tls_verify = true
  boot_wait      = "5s"
  http_directory = "http"
  boot_command = [
    "<wait>c<wait>",
    "linux /casper/vmlinuz --- autoinstall ds=\"nocloud-net;seedfrom=http://{{.HTTPIP}}:{{.HTTPPort}}/\"",
    "<enter><wait>",
    "initrd /casper/initrd ",
    "<enter><wait>",
    "initrd",
    "<enter><wait>",
    "boot",
    "<enter>"
  ]

  vm_name    = "${var.vm_name}"
  vm_id      = "${var.vm_id}"
  memory     = 2048
  cores      = 1
  sockets    = 1
  os         = "l26"
  qemu_agent = true

  disks {
    type              = "scsi"
    disk_size         = "15G"
    storage_pool      = "local-lvm"
    storage_pool_type = "lvm"
    format            = "raw"
  }

  network_adapters {
    bridge   = "vmbr0"
    model    = "virtio"
    firewall = false
  }

  cloud_init = true
  cloud_init_storage_pool = "local-lvm"

}

build {
  name = "${var.template_name}"
  sources = ["source.proxmox-iso.${var.template_name}"]
  provisioner "shell" {
    inline = [
      "ls /",
      "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done",
      "echo haha",
      /* "sudo truncate -s 0 /etc/machine-id", */
      /* "sudo rm /etc/ssh/ssh_host_*", */
      /* "sudo apt-get -y autoremove --purge", */
      /* "sudo apt-get -y clean", */
      /* "sudo apt-get -y autoclean", */
      /* "sudo cloud-init clean", */
      /* "sudo rm -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking-cfg", */
      /* "sudo sync", */
    ]
  }

}
